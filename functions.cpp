#include <iostream>
#include "account.h"
#include "actions.h"

int options()
{
	std::cout << "PLEASE, CHOSE AN OPTION:\n";
	std::cout << "Chose " << PRINT << " to get a list of accounts\n";
	std::cout << "Chose " << ACCOUNTS_COUNT << " to calcule number of accounts\n";
	std::cout << "Chose " << SHOW_ACCOUNT_BY_ID << " to get balance for chosen account\n";
	std::cout << "Chose " << CHANGE_ACCOUNT << " to change current balance\n";
	std::cout << "Chose " << DELETE_ACCOUNT << " to delete account\n";
	std::cout << "Chose " << ADD_ACCOUNT_BY_ID << " to add account using index\n";
	//std::cout << "Chose " << REVERSE_LIST << " to reverse account\n";
	std::cout << "Chose " << EXIT << " if you want finish\n";
		
	int option = 0;
	std::cin >> option;
	std::cout << "\n";
	
	return option;
}

Account* createAccount(double balance)
{
	Account* account = new Account;
	account->balance = balance;
	account->next = nullptr;
	return account;
}

void printAccounts(Account* head)
{
	int index = 0;
	while(head != nullptr)
	{
		std::cout << "Index: " << index << " Balance:" << head->balance << "\n";
		head = head->next;
		++index;
	}
}

Account* addAccountById(Account* head)
{
	int index = 0;
	std::cout << "Enter index for new account\n";
	std::cin >> index;
	
	Account* tempHead = head;
	Account* newAccount =  createAccount(15);	
	Account* nextAccount = nullptr;
	
	int tempIndex = 0;
	
	while (tempHead->next != nullptr)
	{
		if(index == 0)
		{
			head = newAccount;
			newAccount->next = tempHead;
			break;
		}
		else
		{
			if (tempIndex == (index-1) && index != 0)
			{
				nextAccount = tempHead->next;
				tempHead->next = newAccount;
				newAccount->next = nextAccount;
				break;
			}
		}
		tempHead = tempHead->next;
		++tempIndex;
	}
	
	return head;
}

void changeAccount(Account* head)
// why 6 000 000 not in decimal?
{
	int index = 0;
	std::cout << "Enter index\n";
	std::cin >> index;
	double money = 0;
	std::cout << "Enter money\n";
	std::cin >> money;
	
	int tempIndex = 0;
	
	while(head != nullptr)
	{
		if (index == tempIndex)
		{
			head->balance = money;
			break;
		}
	
		head = head->next;
		tempIndex++;
	}
}

int numberOfAccounts(Account* head)
{
	int count = 0;
	while (head != nullptr)
	{
		++count;
		head = head->next;
	}
	
	return count;
}

void showAccountById(Account* head)
{
	int index = 0;
	std::cout << "Enter index\n";
	std::cin >> index;
	
	int tempIndex = 0;
	
	while(head != nullptr)
	{
		if (index == tempIndex)
		{
			std::cout << "Chosen account is " << head->balance << "\n";
			break;
		}
		head = head->next;
		tempIndex++;
	}
}

Account* removeAccount(Account* head)
{
	int index = 0;
	std::cout << "Enter index\n";
	std::cin >> index;
	
	Account* prevAccount = nullptr;
	Account* removeAccount = nullptr;
	Account* nextAccount = nullptr;
	Account* tempHead = head;
	int tempIndex = 0;
	
		while(tempHead->next != nullptr)
		{
			if ((tempIndex == (index-1)) && (index != 0))
			{
				prevAccount = tempHead;
				removeAccount = prevAccount->next;
				nextAccount = removeAccount->next;
				prevAccount->next = nextAccount;
				delete removeAccount;
				break;
			}
			else
			{
				if(index == 0)
				{
					tempHead = head->next;
					delete head;
					head = tempHead;
					break;
				}
			}
			
			tempHead = tempHead->next;
			++tempIndex;
		}
		
	return head;
}

void deleteAllAccounts(Account* head)
{
	while (head->next != nullptr)
	{
		Account* temp = head->next;
		delete head;
		head = temp;
	}
}