#include <iostream>
#include "account.h"
#include "actions.h"
#include "functions.h"

void addAccount(Account* head, double balance)
{
	Account* lastAccount = head;
	
	while(lastAccount->next != nullptr)
	{
		lastAccount = lastAccount->next;
	}
	
	Account* newAccount = createAccount(balance);
	lastAccount->next = newAccount;

}

int main(int argc, char** argv)
{
	Account* head = createAccount(100);
	addAccount(head, 200);
	addAccount(head, 300);
	addAccount(head, 400);
	addAccount(head, 500);
	
	int option = 0;
	printAccounts(head);

while (option != EXIT)
{
	
	option = options();
	
	switch(option)
	{
		case PRINT:
		{
			printAccounts(head);
			std::cout << "\n";
			break;
		}   
		case ACCOUNTS_COUNT:
		{
			std::cout << "Number of accounts is " << numberOfAccounts(head) << "\n";
			std::cout << "\n";
			break;
		}
		case SHOW_ACCOUNT_BY_ID:
		{
			showAccountById(head);
			std::cout << "\n";
			break;
		}
		case CHANGE_ACCOUNT:
		{
			changeAccount(head);
			printAccounts(head);
			std::cout << "\n";
			break;
		}
		case DELETE_ACCOUNT:
		{
			head = removeAccount(head); 
			printAccounts(head);
			std::cout << "\n";
			break;
		}
		case ADD_ACCOUNT_BY_ID:
		{
			head = addAccountById(head);
			printAccounts(head);
			std::cout << "\n";
			break;
		}
		/*case REVERSE_LIST:
		{
			head = reverseList(head);
			printAccounts(head);
			std::cout << "\n";
			break;
		}*/
		case EXIT:
		{
			std::cout << "Have a good day!\n";
			std::cout << "\n";
			break;
		}
		
	}
}
	
	deleteAllAccounts(head);
	return 0;

}