/*#include <iostream> */

Account* createAccount(double balance)
{
	Account* account = new Account;
	account->balance = balance;
	account->next = nullptr;
	return account;
}

void addAccount(Account* head, double balance)
{
	Account* lastAccount = head;
	
	while(lastAccount->next != nullptr)
	{
		lastAccount = lastAccount->next;
	}
	
	Account* newAccount = createAccount(balance);
	lastAccount->next = newAccount;

}