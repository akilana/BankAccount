int options();
Account* createAccount(double balance);
void printAccounts(Account* head);
Account* addAccountById(Account* head);
void changeAccount(Account* head);
int numberOfAccounts(Account* head);
void showAccountById(Account* head);
Account* removeAccount(Account* head);
void deleteAllAccounts(Account* head);