#ifndef ACTIONS_H

#define PRINT 1
#define ACCOUNTS_COUNT 2
#define SHOW_ACCOUNT_BY_ID 3
#define CHANGE_ACCOUNT 4
#define DELETE_ACCOUNT 5
#define ADD_ACCOUNT_BY_ID 6
#define REVERSE_LIST 7
#define EXIT 9

#endif /* ACTIONS_H */