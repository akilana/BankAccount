#ifndef ACCOUNT_H
#define ACCOUNT_H

struct Account
{
	double balance;
	Account* next;
};

#endif /* ACCOUNT_H */